#!/bin/bash

: "${REPO_TOKEN?please define the env variable REPO_TOKEN}"
: "${CI_API_V4_URL?the variable CI_API_V4_URL is undefined. This script shoud be call with gitlab CI context}"
: "${CI_PROJECT_ID?the variable CI_PROJECT_ID is undefined. This script shoud be call with gitlab CI context}"
: "${CI_SERVER_URL?the variable CI_SERVER_URL is undefined. This script shoud be call with gitlab CI context}"

DEFAULT_WAIT_TIMEOUT=120
DEFAULT_RUNNER_TAG="airunner"
REGISTER_RUNNER_TOKEN=`curl --fail --silent --header "Private-Token: ${REPO_TOKEN}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq ".runners_token" -r`
GITLAB_CONFIG_FILE="/workspace/.gitlab-runner/config.toml"

gitlab-runner register --non-interactive --tag-list "${RUNNER_TAG:-DEFAULT_RUNNER_TAG}" \
    -u ${CI_SERVER_URL} -r ${REGISTER_RUNNER_TOKEN} --name ${HOSTNAME} --executor shell

echo "\n" \
     "!! The runner ${HOSTNAME} has been successfully registered and will serve " \
     "the job tagged [${RUNNER_TAG:-DEFAULT_RUNNER_TAG}]." \
     "\n"

RUNNER_TOKEN=`cat ${GITLAB_CONFIG_FILE} | dasel -r toml 'runners.[0].token'`

gitlab-runner run-single --wait-timeout "${RUNNER_WAIT_TIMEOUT:-$DEFAULT_WAIT_TIMEOUT}" \
    -u ${CI_SERVER_URL} -t ${RUNNER_TOKEN} --name ${HOSTNAME} --executor shell

gitlab-runner unregister  -u ${CI_SERVER_URL} -t ${RUNNER_TOKEN}