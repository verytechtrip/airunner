# AIRunner

This git repository hosts the image **airunner** designed to run a **gitlab-runner**
on OVH [AI Training](https://www.ovhcloud.com/en/public-cloud/ai-training/). The runner can then be used for machine learning model training tasks.

The [mlops project](https://gitlab.com/verytechtrip/mlops) presented at the [VeryTechTrip 2023 conference](https://verytechtrip.com) gives an example of using the **AIRunner** runner.

This image is based on [tensorflow-gpu](https://www.tensorflow.org/install/docker?hl=fr) and it embeds some python libraries (numpy, scikit-learn, matplotlib and pyaml) and the tools [dvc](https://dvc.org/) and [cml](https://cml.dev/).

## Usage

In order to use the **ovhai** client in your CI, you must create an ovhai user ([guide](https://docs.ovh.com/ca/en/publiccloud/ai/users/)) and define this variables in your [CI Settings](https://docs.gitlab.com/ee/ci/variables/#for-a-project) :
* *OVHAI_USER*
* *OVHAI_PASSWORD*

You need to pass some environment variables for the image to start properly and find your gitlab project:

* *REPO_TOKEN* : This variable should be define in your [CI Settings](https://docs.gitlab.com/ee/ci/variables/#for-a-project) and must contain [a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the 'api', 'read_repository' and 'write_repository' scopes ;
* *CI_SERVER_URL* : This is a gitlab predefined variable ;
* *CI_API_V4_URL* : This is a gitlab predefined variable ;
* *CI_PROJECT_ID* : This is a gitlab predefined variable.

these variables are optional :
* *RUNNER_TAG* : This is a label for the runner.  It must match the *tag* defined in the training step of your gitlab-ci.yml. The default value is `airunner`;
* *WAITING_TIME* : Seconds to wait for jobs before terminating. The default value is `120`.

You must launch the container from your gitlab.ci file. For example:
```yaml
stages:
  - launch
  - train

launch-runner:
  stage: launch
  before_script:
    - curl https://cli.gra.training.ai.cloud.ovh.net/install.sh | bash
    - /root/bin/ovhai login -u ${OVHAI_USER} -p ${OVHAI_PASSWORD}
  script:
    - |
      /root/bin/ovhai job run \
        --flavor ai1-1-gpu \
        --gpu 1 \
        --env CI_SERVER_URL="$CI_SERVER_URL"  \
        --env CI_API_V4_URL="$CI_API_V4_URL" \
        --env CI_PROJECT_ID="$CI_PROJECT_ID" \
        --env REPO_TOKEN="$REPO_TOKEN" \
        --env WAITING_TIME="60" \
        --env RUNNER_TAG="airunner-gpu" \
        "registry.gitlab.com/verytechtrip/airunner:1.0.0"
```

Then you can use it in a training stage. For example:
```yaml
train:
  stage: train
  needs: [launch-runner]
  tags: [airunner-gpu]
  script:
    - pip install -r requirements.txt
    - python train.py
  artifacts:
    paths:
      - eval/
      - models/
```