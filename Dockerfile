# Great inspirations from:
# https://github.com/ovh/ai-training-examples
# https://github.com/MaximeWeyl/ai-images-sources

ARG TF_VERSION="2.9.3-gpu"
FROM tensorflow/tensorflow:$TF_VERSION

# Change default shell
RUN chsh -s /bin/bash
ENV SHELL=/bin/bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Install usefull tools
RUN apt-get -qy update && apt-get install -qy \
    man \
    vim \
    nano \
    htop \
    curl \
    wget \
    rsync \
    ca-certificates \
    git \
    zip \
    procps \
    ssh \
    gettext-base \
    transmission-cli \
    jq \
    gitlab-runner \
    && apt-get -qq clean \
    && rm -rf /var/lib/apt/lists/*

# install the command script
COPY gitlab-runner.sh /usr/bin/gitlab-runner.sh
RUN chmod a+rx /usr/bin/gitlab-runner.sh 
CMD ["/usr/bin/gitlab-runner.sh"]
ENTRYPOINT []

# install dasel / used by gitlab-runner.sh to parse toml
RUN curl -sSLf "$(curl -sSLf https://api.github.com/repos/tomwright/dasel/releases/latest | grep browser_download_url | grep linux_amd64 | grep -v .gz | cut -d\" -f 4)" -L -o dasel && \
    chmod +x dasel && \
    mv dasel /usr/local/bin

# Create a HOME dedicated to the ovhcloud user (42420:42420)
ENV WORKSPACE_DIR=/workspace

RUN mkdir $WORKSPACE_DIR && \
    chown 42420:42420 $WORKSPACE_DIR && \
    addgroup --gid 42420 ovh && \
    useradd --uid 42420 -g ovh --shell /bin/bash -d $WORKSPACE_DIR ovh && \
    echo "Configuring bash sessions" && \
    echo "if [ -f ~/.bashrc ]; then . ~/.bashrc ; fi" > $WORKSPACE_DIR/.bash_profile && \
    chown 42420:42420 $WORKSPACE_DIR/.bash_profile

# install nodes and python dependancies in user mode
# install nvm
# https://github.com/creationix/nvm#install-script

USER ovh
ENV WORKSPACE_DIR=/workspace
WORKDIR $WORKSPACE_DIR

RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

ENV NVM_DIR=$WORKSPACE_DIR/.nvm

# install node, npm and cml
ARG NODE_VERSION="v16.19.0"
ARG CML_VERSION="0.18.7"

RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default \
    && npm install @dvcorg/cml@$CML_VERSION

# install dvc and some usefull python libraries
ARG DVC_VERSION="2.42.0"
RUN echo "Installing dvc " && \
    pip install --upgrade pip && \
    pip install dvc==$DVC_VERSION \
        dvc-s3 \
        dvclive \
        scikit-learn \
        matplotlib \
        pyaml
    
# By default, tensorflow prints a lot of logs including INFO log that looks like warnings and
# errors in the console/notebooks. We disable logs of INFO level and only keep WARNING and ERROR.
# That results in much more readable notebooks.
# This fix is included even in non tensorflow images, because
# tensorflow may be installed later by the user, and is a really common framework on our platform.
ENV TF_CPP_MIN_LOG_LEVEL=1

# configure PATH
ENV NODE_PATH=$NVM_DIR/versions/node/$NODE_VERSION/bin
ENV NODE_MODULE=$WORKSPACE_DIR/node_modules
ENV PATH=$NODE_PATH:$NODE_MODULE/.bin:$WORKSPACE_DIR/.local/bin:$PATH
ENV HOME=$WORKSPACE_DIR
WORKDIR $WORKSPACE_DIR
